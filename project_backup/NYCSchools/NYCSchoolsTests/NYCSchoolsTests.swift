//
//  NYCSchoolsTests.swift
//  NYCSchoolsTests
//
//  Created by Shujat Ali on 11/10/2023.
//

import XCTest
@testable import NYCSchools
@testable import ApiManager

final class NYCSchoolsTests: XCTestCase {
    
    let viewModel = ListViewModel()
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() async throws {
        do {
            try await viewModel.fetchSchools()
            XCTAssertEqual(viewModel.schoolLists.first?.schoolName, "A. Philip Randolph Campus High School 06M540")
        } catch {
            XCTAssertEqual(error.localizedDescription, "", error.localizedDescription)
        }
        
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    func testListItems() async throws {
        let api = ApiClientAdapter(api: TestApiManager())
        let listItems = try await api.listItems
        XCTAssertEqual(listItems.count, 440)
        XCTAssertEqual(listItems.first?.schoolName, "Clinton School Writers & Artists, M.S. 260")
        
    }

    func testDetailItems() async throws {
        
        let api = ApiClientAdapter(api: TestApiManager())
        let detailItems = try await api.detailItems
        
        XCTAssertEqual(detailItems.count, 478)
        XCTAssertEqual(detailItems.first?.schoolName, "HENRY STREET SCHOOL FOR INTERNATIONAL STUDIES")
        
    }
}


class TestApiManager: ApiManager {

    func data(from url: URL) async throws -> (Data, URLResponse) {
        try await data(from: url, delegate: nil)
    }

    func data(from url: URL, delegate: URLSessionTaskDelegate?) async throws -> (Data, URLResponse) {
        try await Task {
            let fileExt = url.pathExtension
            let resourceName = url.lastPathComponent.replacingOccurrences(of: "." + fileExt, with: "")
            
            guard let fileUrl = Bundle(for: Self.self).url(forResource: resourceName, withExtension: fileExt) else {
                return (Data(), HTTPURLResponse.notFound(url: url))
            }
            let data = try Data(contentsOf: fileUrl)
            return (data, HTTPURLResponse.ok(url: url))
        }.value
    }
}

private extension HTTPURLResponse {

    static func ok(url: URL) -> Self {
        self.init(url: url, statusCode: 200, httpVersion: nil, headerFields: nil)!
    }

    static func notFound(url: URL) -> Self {
        self.init(url: url, statusCode: 404, httpVersion: nil, headerFields: nil)!
    }
}

