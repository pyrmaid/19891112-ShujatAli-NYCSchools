//
//  ListViewModel.swift
//  NYCSchoolAssignment
//
//  Created by Shujat Ali on 13/10/2023.
//

import Foundation
import ApiManager

//Test change
struct ItemViewModel: Identifiable {
    let id = UUID()
    let schoolItem: ListItem
    let detailItem: ListItemDetail?
    
    var schoolName: String {
        "\(schoolItem.schoolName) \(schoolItem.dbn)"
    }
    
    var addressString: String { "\(schoolItem.primaryAddressLine1), \(schoolItem.city), \(schoolItem.stateCode), \(schoolItem.zip)" }
    var overviewParagraph: String { schoolItem.overviewParagraph }

    var website: String { schoolItem.website }
    var websiteURL: URL? {
        if website.lowercased().hasPrefix("http") {
            return URL(string: website)
        } else {
            return URL(string: "http://\(website)")
        }
    }
    var email: String? { schoolItem.schoolEmail }
    var emailURL: URL? { email.flatMap { URL(string: "mailto:\($0)") } }
    var phone: String { schoolItem.phoneNumber }
    var phoneURL: URL? { URL(string: "tel:\(phone)") }
    
    
    //detail part
    var numOfSatTestTakers: String { detailItem?.numOfSatTestTakers ?? "-" }
    var satCriticalReadingAvgScore: String { detailItem?.satCriticalReadingAvgScore ?? "-" }
    var satMathAvgScore: String { detailItem?.satMathAvgScore ?? "-" }
    var satWritingAvgScore: String { detailItem?.satWritingAvgScore ?? "-" }

}

final class ListViewModel: ObservableObject {
    
    @Published
    private(set) var schoolLists: [ItemViewModel] = []
    
    func fetchSchools() async throws {
        
        try await withThrowingTaskGroup(of: ([ListItem], [ListItemDetail]).self) { taskGroup in
            let api = ApiClientAdapter()

            taskGroup.addTask { (try await api.listItems, []) }
            taskGroup.addTask { ([], try await api.detailItems) }

            var schools: [ListItem] = []
            var satScores: [String: ListItemDetail] = [:]
            for try await result in taskGroup {
                schools.append(contentsOf: result.0)
                for satScore in result.1 {
                    satScores[satScore.dbn] = satScore
                }
            }

            
            await MainActor.run { [schools, satScores, weak self] in
                self?.schoolLists = Self.sorted(schools.map {
                    ItemViewModel(schoolItem: $0, detailItem: satScores[$0.dbn])
                })
            }
        }
    }
    
    private static func sorted<C>(
        _ schools: C
    ) -> [ItemViewModel] where C: Sequence, C.Element == ItemViewModel {
        schools.sorted { $0.schoolName < $1.schoolName }
    }
}



