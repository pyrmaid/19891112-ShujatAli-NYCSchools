//
//  NewsListView.swift
//  NYCSchools
//
//  Created by Shujat Ali on 11/10/2023.
//

import SwiftUI

//If got more time cell items could be more improve here
//Mark: - List Row Item
struct NewsItemView: View {
    let item: ItemViewModel
    var body: some View {
        NavigationLink(destination: DetailItemView(item: item)) {
            VStack(alignment: .leading) {
                Text(item.schoolName)
                Text(item.addressString).font(.caption)
                    
            }
        }
    }
}

//Mark: - Main List View
struct NewsListView: View {
    
    @ObservedObject var viewModel: ListViewModel = ListViewModel()
    
    var body: some View {
        NavigationView {
            List(viewModel.schoolLists) { item in
                NewsItemView(item: item)
            }
            .navigationBarTitle("School Lists", displayMode: .inline)
        }
        .task {
            do {
                try await viewModel.fetchSchools()
            } catch {
                
            }
            
        }
    }
}


//Mark: - List Detail Item
struct DetailItemView: View {
    @State var item: ItemViewModel
    var body: some View {
        ScrollView {
            VStack(alignment: .leading) {
                Text(item.schoolName)
                    .font(.title)
                    .fontWeight(.semibold)
                Text(item.addressString)
                    .font(.subheadline)
                    .padding(.bottom, 20)
                
                Text("SAT Scores")
                    .font(.title3)
                    .fontWeight(.bold)
                HStack {
                    Label("Math Avg Score", systemImage: "x.squareroot")
                        .foregroundColor(.blue)
                    Spacer()
                    Text(item.satMathAvgScore)
                }
                Divider()
                HStack {
                    Label("Critical Reading Avg Score", systemImage: "bolt.badge.automatic")
                        .foregroundColor(.blue)
                    Spacer()
                    Text(item.satCriticalReadingAvgScore)
                }
                Divider()
                HStack {
                    Label("Writing Avg Score", systemImage: "pencil.and.scribble")
                        .foregroundColor(.blue)
                    
                    Spacer()
                    Text(item.satWritingAvgScore)
                }
                Divider()
                HStack {
                    Label("No of sat Test Takers", systemImage: "sum")
                        .foregroundColor(/*@START_MENU_TOKEN@*/.blue/*@END_MENU_TOKEN@*/)
                    Spacer()
                    Text(item.satWritingAvgScore)
                }
                
                Divider()
                
                if let web = item.websiteURL {
                    Link(item.website, destination: web)
                }
                
                if let phone = item.phoneURL {
                    Link(item.phone, destination: phone)
                }
                
                if let email = item.emailURL {
                    Link(item.email!, destination: email)
                }
                
                
                Text("Overview")
                    .font(.title3)
                    .fontWeight(.bold)
                Divider()
                Text(item.overviewParagraph)
                
                
            }.padding()
        }
    }
}

#Preview {
    NewsListView()
}
