//
//  NYCSchoolsApp.swift
//  NYCSchools
//
//  Created by Shujat Ali on 11/10/2023.
//

import SwiftUI

@main
struct NYCSchoolsApp: App {
    var body: some Scene {
        WindowGroup {
            NewsListView()
        }
    }
}
