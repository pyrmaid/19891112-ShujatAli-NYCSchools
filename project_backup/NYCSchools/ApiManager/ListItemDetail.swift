//
//  SchoolItemDetailData.swift
//  ApiManager
//
//  Created by Shujat Ali on 13/10/2023.
//

import Foundation

public struct ListItemDetail: Decodable {
    public let dbn: String
    public let numOfSatTestTakers: String
    public let satCriticalReadingAvgScore: String
    public let satMathAvgScore: String
    public let satWritingAvgScore: String
    public let schoolName: String
}
