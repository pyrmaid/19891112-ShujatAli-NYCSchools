//
//  SchoolItemData.swift
//  ApiManager
//
//  Created by Shujat Ali on 13/10/2023.
//

import Foundation

public struct ListItem: Decodable {
    public let city: String
    public let dbn: String
    public let finalgrades: String
    public let neighborhood: String
    public let overviewParagraph: String
    public let phoneNumber: String
    public let primaryAddressLine1: String
    public let schoolEmail: String?
    public let schoolName: String
    public let stateCode: String
    public let totalStudents: String
    public let website: String
    public let zip: String
}
