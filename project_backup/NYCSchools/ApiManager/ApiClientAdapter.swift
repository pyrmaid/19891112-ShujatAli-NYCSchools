//
//  ApiClientAdapter.swift
//  ApiManager
//
//  Created by Shujat Ali on 13/10/2023.
//

import Foundation
import Combine


let BASE_URL = "https://data.cityofnewyork.us/resource"
public struct ApiClientAdapter {
    
    private let api: ApiManager
    
    public init(api: ApiManager = URLSession.shared) {
        self.api = api
    }
    
    public var listItems: [ListItem] {
        get async throws {
            try await callGlobalApi(ofType: [ListItem].self, named: "s3k6-pzi2")
        }
    }

    public var detailItems: [ListItemDetail] {
        get async throws {
            try await callGlobalApi(ofType: [ListItemDetail].self, named: "f9bf-2cp4")
        }
    }
    
    private func callGlobalApi<D: Decodable>(ofType type: D.Type, named name: String) async throws -> D {
        guard let url = URL(string: BASE_URL.appending("/\(name).json")) else {
            throw URLError(.badURL)
        }
        
        let (data, _) = try await api.data(from: url)
        let decoder = JSONDecoder()
        decoder.keyDecodingStrategy = .convertFromSnakeCase
        return try decoder.decode(type, from: data)
    }
}

